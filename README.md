Forked from [https://github.com/pbirnzain/boost_py_autotools](https://github.com/pbirnzain/boost_py_autotools)

Removed a few unnecessary bits and updated a few scripts.
# Original README

Boost::Python currently seems to be the most advanced
and easiest method to use C++ together with Python.
However, it takes some time to set everything up using
autotools instead of boost-build, so I've published my template here.

To build the initial example, run ./initialize.sh.
For most subsequent modifications, autoreconf && ./configure
should suffice.

This is based on Philipp Sterne's work here:
http://www.inference.phy.cam.ac.uk/pjs67/boost_python_autotools.html
